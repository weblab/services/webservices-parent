# WebLab WebServices Parent

This project only contains a parent pom that could be used when building a new WebLab WebService


# Build status
[![build status](https://gitlab.ow2.org/weblab/services/webservices-parent/badges/master/build.svg)](https://gitlab.ow2.org/weblab/services/webservices-parent/commits/master)

# How to build it

In order to be build it, you need to have access to the Maven dependencies it is using. Most of the dependencies are in the central repository and thus does not implies specific configuration.
However, the WebLab Core dependencies are not yet included in the Maven central repository but in a dedicated one that we manage ourselves.
Thus you may have to add the repositories that are listed in the settings.xml. 

# How to use it

## Use this as parent in your pom.xml
```xml
	<parent>
		<groupId>org.ow2.weblab.webservices</groupId>
		<artifactId>parent</artifactId>
		<version>1.2.9-SNAPSHOT</version>
		<relativePath />
	</parent>
```
